import { SimpleCalculator } from './app.po';

describe('simple-calculator-app App', () => {
  let page: SimpleCalculatorPage;

  beforeEach(() => {
    page = new SimpleCalculator();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
