import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DisplayComponent } from './calculator/display/display.component';
import { CalcButtonComponent } from './calculator/button/calcButton.component';
import { DigitComponent } from './calculator/button/digit.component';
import { OperatorComponent } from './calculator/button/operator.component';
import { CalculatorComponent } from './calculator/calculator.component';

@NgModule({
  declarations: [
    AppComponent, CalculatorComponent, DisplayComponent, CalcButtonComponent, DigitComponent, OperatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
