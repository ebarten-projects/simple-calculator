import { Component } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { CalcButtonComponent} from './button/calcButton.component';
import { DisplayComponent} from './display/display.component';

@Component({
    selector: 'calc',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {
    result : number = 0;
    formula = "";
    strNumber = "";
    lastUserChoiceOperator = false;

    @ViewChild(DisplayComponent)
    private calcDisplay : DisplayComponent;

    /**
     * The method processes a button press event.
     * @param button The button pressed class.
     */
    buttonPressed(button: CalcButtonComponent){
        var buttonValue = button.getValue();
        var buttonType = button.getType();
        if (buttonType === "operator") {
            // Operator button pressed - perform calculations and update display
            this.operatorPressed(buttonValue);
        } else if (buttonType === "digit") {
            // A digit's button pressed - perform calculations and update display
            this.digitPressed.call(this, buttonValue);
        } else {
            // Generic button
            console.log("ERROR - undefined button pressed!!!");
            this.lastUserChoiceOperator = false;
        }

    }

    /**
     * The method performs the required calculations and updates the display on the event that the passed button value
     * is an operator button's value.
     * @param buttonValue The actual operator's value.
     */
    operatorPressed(buttonValue) {
        // Check if the user asked to calculate the result
        if (buttonValue === "=") {
            // In case the last user choice action was an operator - apply the formula as the right operand
            if (this.lastUserChoiceOperator) {
                var baseFormula = this.formula.substring(0, this.formula.length - 1);
                this.formula += baseFormula;
            } else {
                // Otherwise - just add the last number to the formula
                this.formula += this.strNumber;
            }
            if (this.formula !== "") {
                // In case the formula is not empty - evaluate it and present it.
                this.result = eval(this.formula);
                this.calcDisplay.updateData("" + this.result);
                this.formula = "";
            }

            this.strNumber = ""; // The number has been added - reset it to be ready to collect the next number's digits.
            this.lastUserChoiceOperator = false;
        } else if (buttonValue === "C") {
            this.result = 0;
            this.formula = "";
            this.strNumber = ""; // Reset user data to be ready to collect the next number's digits.
            this.lastUserChoiceOperator = false;
            this.calcDisplay.updateData("" + this.result);
        } else if (buttonValue === "CE") {
            this.result = 0;
            this.strNumber = ""; // Reset user data to be ready to collect the next number's digits.
            this.calcDisplay.updateData("" + this.result);
        } else {
            if (!this.lastUserChoiceOperator) {
                // The user chose an operator
                if (this.result != 0) {
                    // Previous result already exists use it as the left operand of the formula and reset the number to
                    // be ready to collect the next number's digits
                    this.formula = "" + this.result + buttonValue;
                    this.result = 0; // reset the result
                } else {
                    // no previous calculation made - add the collected number as left operand
                    this.formula += this.strNumber + buttonValue;
                }
                this.strNumber = ""; // Reset the number to be ready to collect the next number's digits.
                this.lastUserChoiceOperator = true;
            }
        }

        // Update the operation on the display.
        this.calcDisplay.updateOperation(buttonValue);
    }

    /**
     * The method performs the required calculations and updates the display on the event that the passed button value
     * is a digit button's value.
     * @param buttonValue The actual digit's value.
     */
    digitPressed(buttonValue) {
        this.result = 0;

        // Check that a decimal point wasn't pressed already
        if (this.strNumber.indexOf('.') === -1) {
            // Number doesn't contain decimal points add the pressed digit
            this.strNumber += buttonValue;
        } else if (buttonValue !== ".") {
            // Button's value is not a decimal point - add the digit
            this.strNumber += buttonValue;
        } else {
            // Button pressed is a decimal point but the number already contains one - ignore it.
        }
        this.calcDisplay.updateData(this.strNumber);
        this.lastUserChoiceOperator = false;
    }

}
