import { Component } from '@angular/core';

@Component({
    selector: 'display',
    templateUrl: './display.component.html',
    styleUrls: ['./display.component.css']
})
export class DisplayComponent {
    data :string = '0';
    operation: string = "none";

    /**
     * The method updates the data part of the display with the passed data.
     * @param data The data to display.
     */
    updateData(data:string) {
        if (data === "") {
            data = '0';
        }
        this.data=data;
    }

    /**
     * The method updates the operator part of the display with the passed operator sign.
     * @param op The operator to display.
     */
    updateOperation(op:string){
        this.operation = op;
    }
}
