import { Component, Input, Output, EventEmitter} from '@angular/core';
import { CalcButtonComponent } from './calcButton.component';

@Component({
    selector: 'digit',
    templateUrl: './calcButton.component.html',
    styleUrls: ['./calcButton.component.css'],
})
export class DigitComponent extends CalcButtonComponent {

    @Output()
    onButtonPressed = new EventEmitter<CalcButtonComponent>()

    buttonPressed(){
        // Make sure the pressed component (e.g. parent) can access this component's data.
        this.onButtonPressed.emit(this);
    }

    getType(){
        return "digit";
    }
}
