import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    templateUrl: './calcButton.component.html',
    styleUrls: ['./calcButton.component.css']
})
export class CalcButtonComponent {

    @Input()
    buttonValue: string = "0";

    getValue() {
        return this.buttonValue;
    }

    getType(){
        return "button";
    }

}
